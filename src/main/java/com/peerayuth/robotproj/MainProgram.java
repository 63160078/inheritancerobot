/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.robotproj;

import java.util.Scanner;

/**
 *
 * @author Ow
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner wn = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map, 100);
        Bomb bomb = new Bomb(5, 5);
        
        map.addObj(new Tree(10,10)); //Priority Low
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(10,9));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(5,10));
        
        map.addObj(new Tree(15,15));
        map.addObj(new Tree(9,15));
        map.addObj(new Tree(15,9));
        map.addObj(new Tree(11,15));
        map.addObj(new Tree(5,15));
        
        map.setBomb(bomb);
        map.setRobot(robot); //Priority High
        
        
        while(true) {
            map.showMap();
            // W,a | N,w | E,d | S,s | q : quit
            char direction = inputDirection(wn);
            if (direction == 'q'){
                printBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printBye() {
        System.out.println("Bye");
    }

    private static char inputDirection(Scanner wn) {
        String str = wn.next();
        return str.charAt(0);
    }
}
